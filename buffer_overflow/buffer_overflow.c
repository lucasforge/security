#include <string.h>
#include <stdlib.h>
#include <stdio.h>

void segredo(void) {
	printf("\n\nVoce acessou uma funcao por BUFFER OVERFLOW!\n\n");
	printf("Segredo\n");
	exit(0);
}

int buffer() {
    char buf[500];
    printf("Entre com o seu nome: ");
    gets(buf);
    printf("Olá, %s!", buf);
    return 0;
}
int main(int argc, char** argv) {
    buffer();
    return 0;
}
